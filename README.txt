INSTRUCTIONS FOR USE:

TESTER:
Open HUP.pd, load a preset and let it run. That's all. Nothing else to do. Sit and enjoy.
As a preset restores nodes and links *but not their former state*, each time you reload a preset you will get different results!

PLAYER:
To rewire without the GUI: manually select object or link to add/remove from the OSC canvas
-You can monitor your changes on the machine by opening Machines and Dynpatcher subpatches. Don't edit them manually though.
-By now you can only use up to 20 simultaneous 2in/2out objects in a patch, freely connected
-Multiple connections to a single inlet just go in (ie they don't mix).
-Only 1 connection from a to b is allowed.
-To route signals to synths: link to object with id=0

To work with the GUI: open first the Pd instance then the OF instance and work from the GUI. Presets must still be loaded from the Pd instance.

POWER USER: 
To add your own machines: use the template template_for_your_nodes.pd provided in the folder /new_nodes and place it, with name xx_shnm.pd (xx the following number after the last node number and shnm any 3 or 4 character short name) on the /n subfolder


